# Lab 4

Docker multi-stage build for [Tour of Heroes](https://angular.io/tutorial/tour-of-heroes) with separate images for different dependency layers.

## Initial build

```shell
docker build -t toh:system --target system .
docker build -t toh:build --target build .
docker build -t toh:app .

docker push toh:system
docker push toh:build
docker push toh:app
```

## Build using `--cache-from`

```shell
docker pull toh:system
docker pull toh:build
docker pull toh:app

docker build -t toh:system --cache-from toh:system --target system .
docker build -t toh:build --cache-from toh:build --target build .
docker build -t toh:app --cache-from toh:app .
```

## Run

```shell
docker run -p 80:80 toh:app
```
